let EmloyeeObject = {
     employees: [
          {
               id: 23,
               name: "Daphny",
               company: "Scooby Doo",
          },
          {
               id: 73,
               name: "Buttercup",
               company: "Powerpuff Brigade",
          },
          {
               id: 93,
               name: "Blossom",
               company: "Powerpuff Brigade",
          },
          {
               id: 13,
               name: "Fred",
               company: "Scooby Doo",
          },
          {
               id: 89,
               name: "Welma",
               company: "Scooby Doo",
          },
          {
               id: 92,
               name: "Charles Xavier",
               company: "X-Men",
          },
          {
               id: 94,
               name: "Bubbles",
               company: "Powerpuff Brigade",
          },
          {
               id: 2,
               name: "Xyclops",
               company: "X-Men",
          },
     ],
};
let fs = require("fs");
function storingfilePromise(employee) {
     let storingPromise = new Promise((resolve, reject) => {
          fs.writeFile("./data.json", JSON.stringify(employee), (err) => {
               if (err) {
                    reject(err);
               } else {
                    resolve("data created successfuly");
               }
          });
     });
     return storingPromise;
}
function readingdata() {
     let readingData = new Promise((resolve, reject) => {
          fs.readFile("./data.json", "utf-8", (err, data) => {
               if (err) {
                    reject(err);
               } else {
                    resolve(data);
               }
          });
     });
     return readingData;
}
function retirvedata(employeedata) {
     let retrivePromise = new Promise((resolve, reject) => {
          let newobject = Object.entries(employeedata)[0][1];
          let id = [2, 13, 23];
          let filterdata = newobject.filter((value) => {
               return id.includes(value.id);
          });
          fs.writeFile(
               "./first.json",
               JSON.stringify(filterdata),
               "utf-8",
               (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         console.log("first problem created successfully");
                         resolve(newobject);
                    }
               }
          );
     });
     return retrivePromise;
}
function groupingdata(employeedata) {
     let groupingPromise = new Promise((resolve, reject) => {
          let reduceddata = employeedata.reduce((acc, curr) => {
               if (acc[curr.company]) {
                    acc[curr.company] = [...acc[curr.company], curr];
               } else {
                    acc[curr.company] = [curr];
               }
               return acc;
          }, {});
          fs.writeFile(
               "./second.json",
               JSON.stringify(reduceddata),
               "utf-8",
               (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         console.log("second problem created successfully");
                         resolve(employeedata);
                    }
               }
          );
     });
     return groupingPromise;
}

function particualarcompany(employeedata) {
     let solvingPromise = new Promise((resolve, reject) => {
          let reduceddata = employeedata.filter((value) => {
               return value.company === "Powerpuff Brigade";
          });
          fs.writeFile(
               "./third.json",
               JSON.stringify(reduceddata),
               "utf-8",
               (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         console.log("third problem created successfully");
                         resolve(employeedata);
                    }
               }
          );
     });
     return solvingPromise;
}
function Removeid(data) {}
storingfilePromise(EmloyeeObject)
     .then((data) => {
          console.log(data);
          return readingdata(data);
     })
     .then((data) => {
          console.log(JSON.parse(data));
          return retirvedata(JSON.parse(data));
     })
     .then((data) => {
          return groupingdata(data);
     })
     .then((data) => {
          return particualarcompany(data);
     })
     .then((data) => {
          return Removeid(data);
     })
     .catch((err) => {
          console.log(err);
     });
